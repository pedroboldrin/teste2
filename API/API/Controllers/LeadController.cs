﻿using Microsoft.AspNetCore.Mvc;
using API.Data;
using API.Models;
using API.MailService;
using System.Linq;
using System.Configuration;
using Microsoft.Extensions.Configuration;
using API.Octopus;

namespace API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class LeadsController : ControllerBase
    {
        private readonly EFContext _context;
        private readonly IMailService _mailService;
        private readonly IMailList _mailList;
        public LeadsController(EFContext context, IMailService mailService, IMailList mailList)
        {
            _context = context;
            _mailService = mailService;
            _mailList = mailList;
        }

        [HttpPost]
        public JsonResult PostLead(Lead lead)
        {
            _context.Lead.Add(lead);


            var campaign = _context.Campaign
                                        .Where<Campaign>(c => c.Name == lead.CampaignName)
                                        .Single();

            _mailList.AddInList(lead);
            _mailService.Send(lead, campaign);
            _context.SaveChanges();

            return new JsonResult(new { success = true });
        }
    }
}
