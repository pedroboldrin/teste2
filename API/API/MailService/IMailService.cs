﻿using API.Models;
using System.Threading.Tasks;

namespace API.MailService
{
    public interface IMailService
    {
        public void Send(Lead lead, Campaign campaign);
    }
}
