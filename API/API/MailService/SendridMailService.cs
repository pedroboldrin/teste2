﻿using API.Models;
using SendGrid;
using SendGrid.Helpers.Mail;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.Extensions.Configuration;
using System.IO;
using System.Linq;
using Microsoft.AspNetCore.Mvc;

namespace API.MailService
{
    public class SendridMailService : IMailService
    {
        private IConfiguration _configuration;
        private string _sendgrid;
        private string _sender;
        private string _name;
        private string _campaignName;
        private string _emailFrom;
        private string _nameFrom;
        private string _subjectEmail;
        public SendridMailService()
        {
            var builder = new ConfigurationBuilder()
                                    .SetBasePath(Directory.GetCurrentDirectory())
                                    .AddJsonFile("appsettings.json");
            var configuration = builder.Build();

            _sendgrid = configuration["Variables:Sendgridkey"];
        }

        public async void Send(Lead lead, Campaign campaign)
        {
            SendridMailService sendridMail = new SendridMailService();
            _sender = lead.Email;
            _name = lead.Name;
            _campaignName = campaign.Name;
            _emailFrom = campaign.EmailFrom;
            _nameFrom = campaign.EmailFromName;
            _subjectEmail = campaign.SubjectEmail;
            var key = sendridMail._sendgrid;
            string apiKey = Environment.GetEnvironmentVariable(key);
            var client = new SendGridClient(_sendgrid);
            var from = new EmailAddress(_emailFrom, _nameFrom);
            var subject = _subjectEmail;
            var to = new EmailAddress(_sender, _name);
            var plainTextContent = "The campaign is: " + _campaignName;
            var htmlContent = $"<strong>receita de {_campaignName}!!!</strong>";
            var msg = MailHelper.CreateSingleEmail(from, to, subject, plainTextContent, htmlContent);
            var response = await client.SendEmailAsync(msg).ConfigureAwait(true);
        }
    }
}
