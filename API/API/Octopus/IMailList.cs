﻿using API.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace API.Octopus
{
    public interface IMailList
    {
        public void AddInList(Lead lead);
    }
}
