﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using API.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using RestSharp;

namespace API.Octopus
{
    public class OctopusService : IMailList
    {
        
        private IConfiguration _configuration;
        private string _listId;
        private string _OctopusKey;
        private string _OctopusUrl;
        private string _OctopusUrlFull;
        private string _sender;

        public OctopusService()
        {
            var builder = new ConfigurationBuilder()
                                   .SetBasePath(Directory.GetCurrentDirectory())
                                   .AddJsonFile("appsettings.json");
            var configuration = builder.Build();
            //_listId = configuration["Variables:OctopusListId"];
            _OctopusKey = configuration["Variables:Octopuskey"];
            //_OctopusUrl = configuration["Variables:OctopusUrl"];
            _OctopusUrlFull = configuration["Variables:OctopusUrlFull"];
        }

        public void AddInList(Lead lead)
        {

            OctopusService octopus = new OctopusService();
            _sender = lead.Email;
            var url = octopus._OctopusUrlFull;
            //var url = string.Format(_OctopusUrl, _listId);
            var client = new RestClient(url);
            var request = new RestRequest(Method.POST);
            request.AddHeader("cache-control", "no-cache");
            request.AddHeader("Content-Type", "application/json");
            var apikey = _OctopusKey;
            var body = "{\n    \"api_key\": \""+apikey+"\",\n    \"email_address\": \""+lead.Email+"\",\n    \"status\": \"SUBSCRIBED\"\n}";
            request.AddParameter("undefined", body, ParameterType.RequestBody);
            //request.AddParameter("undefined", "{\n    \"api_key\": \"1a93fd62-1380-11ea-be00-06b4694bee2a\",\n    \"email_address\": \"pedro.firebase@gmail.com\",\n    \"status\": \"SUBSCRIBED\"\n}", _sender,ParameterType.RequestBody);
            IRestResponse response = client.Execute(request);
        }
    }
}
