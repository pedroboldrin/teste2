Create database DBApi
go

Create table Lead
(
	Id				int			not null	primary key identity,
	Email			varchar(50)	not null,
	Name			varchar(50)	not null,
	Telephone		varchar(50) not null,
	CampaignName	varchar(50)	not null,
	Time			datetime	not null
)
go

insert into Lead values('pedro.boldrin@gmail.com','Pedro Boldrin', '(17)991406332', 'Sal Proteinado', '2019-12-02' )
insert into Lead values('fulano@gmail.com','Fulano', '(17)991406332', 'receita para silagem', '2019-12-02' )

select * from lead
go

Create table Campaign
(
	IdCampaign		int			not null	primary key identity,
	Name			varchar(50)	not null,
	EmailFrom		varchar(50)	not null,
	EmailFromName	varchar(50)	not null,
	SubjectEmail	varchar(50)	not null,
	TemplateId		int			not null
)
go

select * from Campaign

insert into Campaign values('Sal Proteinado', 'pedro.boldrin@gmail.com', 'Pedro Boldrin', 'Receita de Sal proteinado', 1)
insert into Campaign values('receita para silagem', 'pedro.boldrin@gmail.com', 'Pedro Boldrin', 'receita para silagem', 1)


select * from lead
select * from Campaign

delete from lead where id=11

-- HubSpot
Create Table hubspot
(
	Id					int				not null		primary key,
	responsibleBusiness	varchar(50)		not null,
	businessValue		decimal(10,2)	not null,
	businessDate		date			not null,
	newSale				decimal(10,2),
	buyback				decimal(10,2)
)
go